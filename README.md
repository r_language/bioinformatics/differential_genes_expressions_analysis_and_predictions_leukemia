In this R file you can have a look at a differential genes expressions analysis made with limma library and leukemia data from leukemiasEset library. Afterwards, we created multiple classifers : 

- Multiclasspairs

- Random Forest

- Pamr

- Random Forest SRC 

We used those classifiers to perform predictions on test data and we compared the different classifiers in the end (accuracy, kappa, top k genes, ...). You can also see the HTML output made with rmarkdown.

Have fun ! 

Author : Marion Estoup 

Mail : marion_110@hotmail.fr

Date : July 2023